/**
 * Created by Dave on 1/9/2016.
 */

var SELECTED_THEME = null;
var PVP_START_DATE = null;
var PVP_END_DATE = null;

function loadCssFile(filename){
    var fileref=document.createElement("link");
    fileref.setAttribute("id", "cssTheme");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

function setFloatBg(selectedTheme){
  if (selectedTheme==="darkly") $(".table.floatThead-table").addClass("floatBgDarkly");
  else if (selectedTheme==="cyborg") $(".table.floatThead-table").addClass("floatBgCyborg");
  else if (selectedTheme==="slate") $(".table.floatThead-table").addClass("floatBgSlate");
  else $(".table.floatThead-table").addClass("floatBgDefault");
}

function initCss(){
    var theme = localStorage.getItem("cssTheme");
    if (!theme) theme = "default";
    SELECTED_THEME = theme;
    if (theme=="default")
        loadCssFile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");
    else
        loadCssFile("https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/"+theme+"/bootstrap.min.css");
    $(".cssSelector").click ( function() {
        var selectedTheme = $(this).attr ( "data-suffix" );
        localStorage.setItem("cssTheme", selectedTheme);
        location.reload();
    });
}

initCss();

function handleUpdatePvpDates(){

  var sStart = $("#pvpRangeStartDate").val();
  if (sStart && sStart.trim().length>0){
    PVP_START_DATE = moment(sStart, "MM/DD/YYYY");
  }
  else PVP_START_DATE = null;

  var sEnd = $("#pvpRangeEndDate").val();
  if (sEnd && sEnd.trim().length>0){
    PVP_END_DATE = moment(sEnd, "MM/DD/YYYY").add(1, 'days');;
  }
  else PVP_END_DATE = null;

  //console.log(PVP_START_DATE+" "+PVP_END_DATE);
}

$(document).ready(function() {
  $(".floatMe").floatThead({
    zIndex: 2,
    responsiveContainer: function($table){
      return $table.closest(".table-responsive");
    }
  });
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(evt){
    $('.floatMe:visible').floatThead('reflow');
    
    //just reload each time even though it'll only matter on pvp tab

    $('#pvpRange').datepicker({
      inputs: $('.actual-range'),
      format: 'mm/dd/yyyy',
      startDate: "09/09/2014",
      endDate: "+1d",
      clearBtn: true
    }).on("changeDate", handleUpdatePvpDates)
      .on("clearDate", handleUpdatePvpDates);
    //
    // $(".actual-range").on("dp.change", function() {
    //   console.log()
    //
    //   $scope.selecteddate = $("#datetimepicker").val();
    //   alert("selected date is " + $scope.selecteddate);
    //
    // });
    
    // $('.input-daterange input').each(function() {
    //   $(this).datepicker({
    //     format: 'mm/dd/yyyy'
    //   });
    // });
    
    $('.datepicker').datepicker({
      format: 'mm/dd/yyyy'
    });
    
  });

  setFloatBg(SELECTED_THEME);
  
});