(function(angular) {
    'use strict';

    angular.module('gearApp')
        .factory('Manifest', Manifest);

    Manifest.$inject = ['$q', '$http'];

    function Manifest($q, $http) {
        var deferred = $q.defer();

        $http.get('js/destiny.json')
            .success(function(data) {
                deferred.resolve(data);
            })
            .error(function() {
                deferred.reject(new Error('The manifest file was not parsed correctly.'));
            });

        return {
            'getDefinitions': function() { return deferred.promise; }
        };
    }
})(angular);
