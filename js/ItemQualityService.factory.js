/**
 * Created by Dave on 12/24/2015.
 */
(function () {
  'use strict';


  angular.module('gearApp')
    .factory('ItemQualityService', ItemQualityService);

  ItemQualityService.$inject = ['$q', '$http', 'toaster'];


  function ItemQualityService($q, $http, toaster) {

    var service = {
      setQualityRating: setQualityRating
    };
    return service;


    function fitValue(light) {
      if (light > 300) {
        return (0.2546 * light) - 23.825;
      } if (light > 200) {
        return (0.1801 * light) - 1.4612;
      } else {
        return -1;
      }
    }

    
    function getScaledStat(base, light) {
      var max = 335;

      if (light > 335) {
        light = 335;
      }

      return {
        min: Math.floor((base) * (fitValue(max) / fitValue(light))),
        max: Math.floor((base + 1) * (fitValue(max) / fitValue(light)))
      };
    }
    

    function getBonus(light, type) {
      switch (type.toLowerCase()) {
        case 'helmet':
        case 'helmets':
          return light < 292 ? 15
            : light < 307 ? 16
            : light < 319 ? 17
            : light < 332 ? 18
            : 19;
        case 'gauntlets':
          return light < 287 ? 13
            : light < 305 ? 14
            : light < 319 ? 15
            : light < 333 ? 16
            : 17;
        case 'chest':
        case 'chest armor':
          return light < 287 ? 20
            : light < 300 ? 21
            : light < 310 ? 22
            : light < 319 ? 23
            : light < 328 ? 24
            : 25;
        case 'leg':
        case 'leg armor':
          return light < 284 ? 18
            : light < 298 ? 19
            : light < 309 ? 20
            : light < 319 ? 21
            : light < 329 ? 22
            : 23;
        case 'classitem':
        case 'class items':
        case 'class armor':
        case 'ghost':
        case 'ghosts':
          return light < 295 ? 8
            : light < 319 ? 9
            : 10;
        case 'artifact':
        case 'artifacts':
          return light < 287 ? 34
            : light < 295 ? 35
            : light < 302 ? 36
            : light < 308 ? 37
            : light < 314 ? 38
            : light < 319 ? 39
            : light < 325 ? 40
            : light < 330 ? 41
            : 42;
      }
      console.warn('item bonus not found', type);
      return 0;
    }
    
    function getSplit(type){
      var split = 0;
      switch (type.toLowerCase()) {
        case 'helmet':
          split = 46; // bungie reports 48, but i've only seen 46
          break;
        case 'gauntlets':
          split = 41; // bungie reports 43, but i've only seen 41
          break;
        case 'chest':
        case 'chest armor':
          split = 61;
          break;
        case 'leg':
        case 'leg armor':
          split = 56;
          break;
        case 'classitem':
        case 'class items':
        case 'class armor':
        case 'ghost':
        case 'ghosts':
          split = 25;
          break;
        case 'artifact':
        case 'artifacts':
          split = 38;
          break;
        default:
          return null;
      }
      return split;
    }
    
    function setQualityRating(item) {
      // For a quality property, return a range string (min-max percentage)
      item.qualityPct = -1;
      var stats = item.stats;
      var light = item.light; 
      var type = item.bucket2;
      
      if (!stats || !stats.length) {
        return;
      }
      var split = getSplit(type);
      if (split==null){
        return;
      }

      var hasI = false, hasD = false, hasS = false;
      var bonus = getBonus(light, type);
      var strBonus = 0, discBonus = 0, intBonus = 0;

      for (var cntr = 0; cntr < item.steps.length; cntr++) {
        var step = item.steps[cntr];
        if ('Increase Intellect' == step.step.nodeStepName) {
          hasI = true;
          if (step.selected) intBonus = bonus;
        }
        else if ('Increase Discipline' == step.step.nodeStepName) {
          hasD = true;
          if (step.selected) discBonus = bonus;
        }
        else if ('Increase Strength' == step.step.nodeStepName) {
          hasS = true;
          if (step.selected) strBonus = bonus;
        }
      }

      if (hasI && hasD) {
        item.statSplit = "ID";}
      else if (hasD && hasS) {
        item.statSplit = "DS";
      }
      else if (hasS && hasI) {
        item.statSplit = "IS";
      }

      if (light < 280){
        return;
      }
      
      var ret = {
        total: {
          min: 0,
          max: 0
        },
        max: split * 2
      };

      var pure = 0;

      for (var cntr = 0; cntr < stats.length; cntr++) {
        var stat = stats[cntr];
        var scaled = {
          min: 0,
          max: 0
        };
        var base = 0;
        if ("Intellect" == stat.name) {
          base = stat.value - intBonus;
        }
        else if ("Discipline" == stat.name) {
          base = stat.value - discBonus;
        }
        else if ("Strength" == stat.name) {
          base = stat.value - strBonus;
        }
        else{
          return;
        }
        stat.base = base;
        
        if (stat.base) {
          scaled = getScaledStat(stat.base, light);
          pure = scaled.min;
        }
        stat.scaled = scaled;
        stat.split = split;
        stat.qualityPercentage = {
          min: Math.round(100 * stat.scaled.min / stat.split),
          max: Math.round(100 * stat.scaled.max / stat.split)
        };
        ret.total.min += scaled.min || 0;
        ret.total.max += scaled.max || 0;
      };

      if (pure === ret.total.min) {
        stats.forEach(function(stat) {
          stat.scaled = {
            min: Math.floor(stat.scaled.min / 2),
            max: Math.floor(stat.scaled.max / 2)
          };
          stat.qualityPercentage = {
            min: Math.round(100 * stat.scaled.min / stat.split),
            max: Math.round(100 * stat.scaled.max / stat.split)
          };
        });
      }

      var quality = {
        min: Math.round(ret.total.min / ret.max * 100),
        max: Math.round(ret.total.max / ret.max * 100)
      };

      if (type.toLowerCase() !== 'artifact') {
        stats.forEach(function(stat) {
          stat.qualityPercentage = {
            min: Math.min(100, stat.qualityPercentage.min),
            max: Math.min(100, stat.qualityPercentage.max)
          };
        });
        quality = {
          min: Math.min(100, quality.min),
          max: Math.min(100, quality.max)
        };
      }

      stats.forEach(function(stat) {
        if (!stat.qualityPercentage) return;
        if ("Intellect" == stat.name) {
          item.intPct = stat.qualityPercentage.min;
        }
        else if ("Discipline" == stat.name) {
          item.disPct = stat.qualityPercentage.min;;
        }
        else if ("Strength" == stat.name) {
          item.strPct = stat.qualityPercentage.min;;
        }
      });
      //item.quality = quality;
      item.qualityPct = quality.min;
      return;
    }


  }
})();