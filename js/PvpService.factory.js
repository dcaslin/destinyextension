/**
 * Created by Dave on 12/24/2015.
 */
(function () {
  'use strict';


  angular.module('gearApp')
    .factory('PvPService', PvpService);

  PvpService.$inject = ['$q', '$http', 'toaster'];

  function PvpService($q, $http, toaster) {
    var apiKey = '985d343f3e0d42a1bdcc1976ea9319a6';

    var service = {
      getPvpHistory: getPvpHistory,
      getPvpHistory2: getPvpHistory2,
      getPGCR: getPGCR,
      getPvpHistoryForChar: getPvpHistoryForChar
    };
    return service;

    function handleErrors(response) {
      if (response.status === 503) {
        return $q.reject(new Error("Bungie.net is down."));
      }
      if (response.status < 200 || response.status >= 400) {
        return $q.reject(new Error('Network error: ' + response.status));
      }

      var errorCode = response.data.ErrorCode;
      if (errorCode === 36) {
        return $q.reject(new Error('Bungie API throttling limit exceeded. Please wait a bit and then retry.'));
      } else if (errorCode === 99) {
        return $q.reject(new Error('Please log into Bungie.net in order to use this extension.'));
      } else if (errorCode === 5) {
        return $q.reject(new Error('Bungie.net servers are down for maintenance.'));
      } else if (errorCode === 1618 &&
        response.config.url.indexOf('/Account/') >= 0 &&
        response.config.url.indexOf('/Character/') < 0) {
        return $q.reject(new Error('No Destiny account was found for this platform.'));
      } else if (errorCode > 1) {
        return $q.reject(new Error(response.data.Message));
      }

      return response;
    }


    /************************************************************************************************************************************/

    function getPvpHistoryRequest(platform, membershipId, character, mode, page, count) {
      return {
        method: 'GET',
        url: 'https://www.bungie.net/Platform/Destiny/Stats/ActivityHistory/' + platform + '/'
        + membershipId + '/' + character.id + '/?page=' + page + '&count=' + count + '&mode=' + mode,
        headers: {
          'X-API-Key': apiKey
        }
      };
    }

    function processPvpHistoryResponse(character, response) {
      return {
        character: character,
        activities: response.data.Response.data.activities
      };
    }

    function getPvpHistoryPageForChar(platform, membershipId, character, mode, count, page) {
      console.log("Getting page: " + page);
      return $http({
        method: 'GET',
        url: 'https://www.bungie.net/Platform/Destiny/Stats/ActivityHistory/' + platform + '/'
        + membershipId + '/' + character.id + '/?page=' + page + '&count=' + count + '&mode=' + mode,
        headers: {
          'X-API-Key': apiKey
        }
      }).then(handleErrors)
        .then(function (response) {
          if (response.data.Response.data.activities && response.data.Response.data.activities.length > 0)
            return response.data.Response.data.activities;
          else return null;
        });
    }

    function getPvpHistoryForChar(platform, membershipId, character, mode, count, vm) {
      var MAX_PAGE_SIZE = 100;
      var curPage = 0;

      return new Promise(function (resolve, reject) {
        var allMatches = [];

        function processMatches(results) {
          if (results == null || allMatches.length > count) {
            resolve(allMatches);
          }
          else {
            curPage++;
            if (vm)
              vm.statusMsg = "Retrieving page " + curPage;
            results.forEach(function (r) {
              allMatches.push(r);
            });
            return getPvpHistoryPageForChar(platform, membershipId, character, mode, MAX_PAGE_SIZE, curPage).then(processMatches)
              .catch(function (e) {
                reject(e);
              });
          }
        }

        getPvpHistoryPageForChar(platform, membershipId, character, mode, MAX_PAGE_SIZE, curPage).then(processMatches)
          .catch(function (e) {
            reject(e);
          });
      });
    }

    function getPvpHistoryForChar2(platform, membershipId, char, mode, paramCount) {

      var charAct = {
        character: char,
        activities: []
      };
      var count = paramCount;
      if (count < 1 || count > 100) count = 100;

      var loopMe = function (page, deferred) {
        deferred = deferred || $q.defer();

        $http({
          method: 'GET',
          url: 'https://www.bungie.net/Platform/Destiny/Stats/ActivityHistory/' + platform + '/'
          + membershipId + '/' + char.id + '/?page=' + page + '&count=' + count + '&mode=' + mode,
          headers: {
            'X-API-Key': apiKey
          }
        })
          .then(function (response) {
            if (response.status === 503) {
              return $q.reject(new Error("Bungie.net is down."));
            }
            if (response.status < 200 || response.status >= 400) {
              return $q.reject(new Error('Network error: ' + response.status));
            }
            var errorCode = response.data.ErrorCode;
            if (errorCode === 36) {
              return deferred.reject(new Error('Bungie API throttling limit exceeded. Please wait a bit and then retry.'));
            } else if (errorCode === 99) {
              return deferred.reject(new Error('Please log into Bungie.net in order to use this extension.'));
            } else if (errorCode === 5) {
              return deferred.reject(new Error('Bungie.net servers are down for maintenance.'));
            } else if (errorCode === 1618 &&
              response.config.url.indexOf('/Account/') >= 0 &&
              response.config.url.indexOf('/Character/') < 0) {
              return deferred.reject(new Error('No Destiny account was found for this platform.'));
            } else if (errorCode > 1) {
              return deferred.reject(new Error(response.data.Message));
            }
            return response;
          })
          .then(function (response) {
            var matches = response.data.Response.data.activities;
            if (!matches){
              matches = [];
            }
            var done = true;

            for (var cntr = 0; cntr < matches.length; cntr++) {
              var match = matches[cntr];
              var period = moment(match.period);
              //matches are in reverse chron order
              //too young, keep going
              if (PVP_END_DATE && period.isAfter(PVP_END_DATE)) {
                continue;
              }
              //too old, stop
              else if (PVP_START_DATE && period.isBefore(PVP_START_DATE)) {
                done = true;
                break;
              }
              //full, stop
              else if (paramCount > 0 && charAct.activities.length >= count) {
                done = true;
                break;
              }
              else {
                charAct.activities.push(match);
                done = false;
              }
            }
            if (done === true) {
              deferred.resolve(charAct);
            }
            else {
              loopMe(page + 1, deferred);
            }
          })
          .catch(function (err) {
            deferred.reject(err);
          });

        return deferred.promise;
      };
      return loopMe(0);
    }

    function getPvpHistory2(platform, membershipId, characters, mode, count) {

      console.log(PVP_START_DATE + " " + PVP_END_DATE);
      var charPromises = characters.map(function (char) {
        return getPvpHistoryForChar2(platform, membershipId, char, mode, count);
      });
      return $q.all(charPromises);
    }


    function getPvpHistory(platform, membershipId, characters, mode, count) {
      //TODO filter by date

      console.log(PVP_START_DATE + " " + PVP_END_DATE);
      var MAX_PAGE_SIZE = 100;
      var pages = [];

      while (count > MAX_PAGE_SIZE) {
        pages.push(MAX_PAGE_SIZE);
        count -= MAX_PAGE_SIZE;
      }
      if (count > 0) {
        pages.push(count);
      }

      var charPages = [];
      characters.forEach(function (char) {
        pages.forEach(function (count, index) {
          charPages.push({
            char: char,
            page: index,
            count: count
          });
        });
      });
      var promises = charPages.map(function (charPage) {
        var processPB = processPvpHistoryResponse.bind(null, charPage.char);
        return $q.when(getPvpHistoryRequest(platform, membershipId, charPage.char, mode, charPage.page, charPage.count))
          .then($http)
          .then(handleErrors)
          .then(processPB)
          .catch(function (e) {
            toaster.pop('error', 'Bungie.net Error', e.message);
            return $q.reject(e);
          });
      });
      return $q.all(promises);
    }

    /************************************************************************************************************************************/

    function sendPGCR(game, cache) {
      var deferred = $q.defer();
      var id = game.activityDetails.instanceId;
      if (cache[id]) {
        //check if has fireteam data, if not, can't use
        if (cache[id].fireTeamSize) {
          deferred.resolve(cache[id]);
          return deferred.promise;
        }
      }
      return $http({
        method: 'GET',
        url: 'http://www.bungie.net/Platform/Destiny/Stats/PostGameCarnageReport/' + game.activityDetails.instanceId,
        headers: {
          'X-API-Key': apiKey
        }
      });
    }

    function processPGCRResponse(game, pvpProgress, cache, response) {
      var id = game.activityDetails.instanceId;
      game.weapons = [];

      var fireteams = {};
      var playerEntry = null;
      var missingData = false;
      response.data.Response.data.entries.forEach(function (player, cntr) {
        //ignore everyone else for now
        if (!player) return; //this was deleted from cache and we're good to go
        if (!player.extended) {
          game.missingData = true;
          return;
        }
        if (player.extended.values.fireTeamId) {
          var fireTeamId = player.extended.values.fireTeamId.basic.value;
          if (!fireteams[fireTeamId]) {
            fireteams[fireTeamId] = 0;
          }
          fireteams[fireTeamId]++;
        }
        if (player.characterId != game.character.id) {

          delete response.data.Response.data.entries[cntr];
          return;
        }
        playerEntry = player;
        if (!player.extended.weapons) return;
        player.extended.weapons.forEach(function (weapon) {
          var jDesc = CACHE.InventoryItem[weapon.referenceId];
          if (!jDesc) return;
          game.weapons.push({
            id: weapon.referenceId,
            name: jDesc.itemName,
            icon: jDesc.icon,
            kills: weapon.values.uniqueWeaponKills.basic.value,
            precision: weapon.values.uniqueWeaponPrecisionKills.basic.value,
            precisionPct: weapon.values.uniqueWeaponKillsPrecisionKills.basic.displayValue
          });
        });
      });

      //this wasn't cached, record the fireteam count
      if (playerEntry) {
        if (playerEntry.extended.values.fireTeamId) {
          var fireTeamSize = fireteams[playerEntry.extended.values.fireTeamId.basic.value];
          response.fireTeamSize = fireTeamSize;
        }
        else {
          response.fireTeamSize = 1;
        }
      }
      game.fireTeamSize = response.fireTeamSize;
      pvpProgress.completed++;
      pvpProgress.pct = ((pvpProgress.completed / pvpProgress.total) * 100.0).toFixed(0) + "%";

      delete response.config;
      delete response.headers;
      cache[id] = response;
      cache.dirty = true;
      return response;
    }

    function getPGCR(cache, pvpProgress, game) {
      var processPGCR = processPGCRResponse.bind(null, game, pvpProgress, cache);
      return $q.when(sendPGCR(game, cache))
        .then(handleErrors)
        .then(processPGCR)
        .catch(function (e) {
          toaster.pop('error', 'Bungie.net Error', e.message);
          return $q.reject(e);
        });
    }
  }
})();