var CACHE = null;

var manifest = chrome.runtime.getManifest();
document.title = manifest.short_name + " " + manifest.version;

var BS_ROW_STYLE_PREFIX = "";

(function () {
  'use strict';

  var PGCR_CACHE = {};

  angular.module('gearApp').controller('gearController', GearController);
  GearController.$inject = ['Manifest', 'BungieService', 'PvPService', 'ItemQualityService', 'toaster', 'dimState', '$scope', '$interval', '$q', '$http'];
  function GearController(Manifest, BungieService, PvPService, ItemQualityService, toaster, dimState, $scope, $interval, $q, $http) {

    var TAGS = {
      upgrade: {
        style: BS_ROW_STYLE_PREFIX + "info",
        label: "Upgrade Me"
      },
      keep: {
        style: BS_ROW_STYLE_PREFIX + "success",
        label: "Keep Me"
      },
      test: {
        style: BS_ROW_STYLE_PREFIX + "active",
        label: "Test Me"
      },
      infuse: {
        style: BS_ROW_STYLE_PREFIX + "warning",
        label: "Infusion Fuel"
      },
      junk: {
        style: BS_ROW_STYLE_PREFIX + "danger",
        label: "Dismantle Me"
      }
    };

    var weaponTypeFilters = [
      {label: "All Weapons", filter: null},
      {label: "Primary Weapons", filter: "Primary Weapons"},
      {label: "Special Weapons", filter: "Special Weapons"},
      {label: "Heavy Weapons", filter: "Heavy Weapons"},
      {label: "Postmaster", filter: "Lost Items"}
    ];

    var armorTypeFilters = [
      {label: "All Armor", filter: null},
      {label: "Helmet", filter: "Helmet"},
      {label: "Gauntlet", filter: "Gauntlets"},
      {label: "Chest Armor", filter: "Chest Armor"},
      {label: "Leg Armor", filter: "Leg Armor"},
      {label: "Class Armor", filter: "Class Armor"},
      {label: "Artifact", filter: "Artifacts"},
      {label: "Ghost", filter: "Ghost"},
      {label: "Postmaster", filter: "Lost Items"}
    ];

    var armorEquippableFilters = [
      {label: "Usable By Any", filter: null},
      {label: "Titan", filter: "Titan"},
      {label: "Hunter", filter: "Hunter"},
      {label: "Warlock", filter: "Warlock"},
      {label: "All Classes", filter: "Any"}
    ];

    var rarityFilters = [
      {label: "Any Rarity", filter: null},
      {label: "Exotic", filter: "Exotic"},
      {label: "Legendary", filter: "Legendary"},
      {label: "Rare", filter: "Rare"},
      {label: "The Rest", filter: "Lower"}
    ];


    var modeOptions = [
      {label: "All Modes", filter: "AllPvP"},
      {label: "Skirmish", filter: "ThreeVsThree"},
      {label: "Control", filter: "Control"},
      {label: "Salvage", filter: "Lockdown"},
      {label: "Clash", filter: "Team"},
      {label: "Rumble", filter: "FreeForAll"},
      {label: "Iron Banner", filter: "IronBanner"},
      {label: "Trials", filter: "TrialsOfOsiris"},
      {label: "Elimination", filter: "Elimination"},
      {label: "Rift", filter: "Rift"},
      {label: "Zone Control", filter: "ZoneControl"},
      {label: "Supremacy", filter: "Supremacy"},
      {label: "Private/Custom", filter: "PrivateMatchesAll"}

    ];

    var countOptions = [
      {label: "5", filter: 5},
      {label: "10", filter: 10},
      {label: "20", filter: 20},
      {label: "50", filter: 50},
      {label: "100", filter: 100},
      {label: "200", filter: 200},
      {label: "All", filter: -1}
    ];


    var vm = $scope.vm = {
      dirty: false,
      loading: true,
      version: manifest.version,
      cacheReady: false,
      loggedIn: false,
      marked: {},
      notes: {},
      globalFilters: {
        tag: "any"
      },
      rarityFilters: rarityFilters,
      weaponFilters: {
        type: weaponTypeFilters,
        selectedType: weaponTypeFilters[0],
        selectedRarity: rarityFilters[0],
        selectedOwner: {
          id: null,
          label: "All Owners"
        }
      },
      armorFilters: {
        type: armorTypeFilters,
        selectedType: armorTypeFilters[0],
        selectedRarity: rarityFilters[0],
        selectedOwner: {
          id: null,
          label: "All Owners"
        },
        equippable: armorEquippableFilters,
        selectedEquippable: armorEquippableFilters[0]
      },
      pvpFilters: {
        modeOptions: modeOptions,
        countOptions: countOptions,
        selectedMode: modeOptions[0],
        selectedCount: countOptions[0],
        selectedFireTeam: "any",
        selectedMap: "any",
        selectedChar: {
          id: null,
          label: "All Chars"
        },
      }
    };


    function filterTag(item) {
      if (vm.globalFilters.tag && vm.globalFilters.tag != "any") {
        var mark = $scope.vm.marked[item.instanceId];
        if (vm.globalFilters.tag == "none") {
          if (mark != null) return false;
        }
        else {
          if (vm.globalFilters.tag != mark) return false;
        }
      }
      return true;
    }

    function filterText(item) {
      if (vm.globalFilters.upperText) {
        if (item.searchText.indexOf(vm.globalFilters.upperText) == -1) return false;
      }
      return true;
    }

    $scope.vm.setTextFilter = function (vm) {
      if (vm.globalFilters.text)
        vm.globalFilters.upperText = vm.globalFilters.text.toUpperCase();
      else
        vm.globalFilters.upperText = null;
    }

    $scope.vm.filterItems = function (vm) {
      $('.floatMe:visible').floatThead('reflow');
      if (!vm.gear) return;
      if (vm.curTab == 'weapons' || !vm.curTab) {
        var weapons = vm.gear.gear.weapons;
        for (var cntr = 0; cntr < weapons.length; cntr++) {
          weapons[cntr].hidden = !$scope.vm.filterWeapon(weapons[cntr]);
        }
      }
      else if (vm.curTab == 'armor') {
        var armor = vm.gear.gear.armor;
        for (var cntr = 0; cntr < armor.length; cntr++) {
          armor[cntr].hidden = !$scope.vm.filterArmor(armor[cntr]);
        }
      }
    }

    $scope.vm.filterWeapon = function (item) {
      if (!filterTag(item)) return false;

      if (vm.weaponFilters.selectedType.filter != null) {
        if (vm.weaponFilters.selectedType.filter != item.bucket2 && vm.weaponFilters.selectedType.filter != item.bucket)
          return false;
      }
      if (vm.weaponFilters.selectedOwner && vm.weaponFilters.selectedOwner.id) {
        if (vm.weaponFilters.selectedOwner.id != item.owner.id)
          return false;
      }
      if (vm.weaponFilters.minLight) {
        if (item.light < vm.weaponFilters.minLight) return false;
      }
      if (vm.weaponFilters.maxLight) {
        if (item.light > vm.weaponFilters.maxLight) return false;
      }
      if (vm.weaponFilters.selectedRarity.filter) {
        var filter = vm.weaponFilters.selectedRarity.filter;
        if (filter === "Lower") {
          if (item.tierTypeName === "Exotic" || item.tierTypeName === "Legendary" || item.tierTypeName === "Rare") return false;
        }
        else if (filter != item.tierTypeName) return false;
      }
      if (!filterText(item)) return false;
      return true;
    };

    $scope.vm.filterArmor = function (item) {
      if (!filterTag(item)) return false;

      if (vm.armorFilters.selectedType.filter != null) {
        if (vm.armorFilters.selectedType.filter != item.bucket && vm.armorFilters.selectedType.filter != item.bucket2)
          return false;
      }
      if (vm.armorFilters.selectedOwner && vm.armorFilters.selectedOwner.id) {
        if (vm.armorFilters.selectedOwner.id != item.owner.id)
          return false;
      }
      if (vm.armorFilters.minLight) {
        if (item.light < vm.armorFilters.minLight) return false;
      }
      if (vm.armorFilters.maxLight) {
        if (item.light > vm.armorFilters.maxLight) return false;
      }
      if (vm.armorFilters.selectedEquippable.filter) {
        if (vm.armorFilters.selectedEquippable.filter != item.classAllowed)
          return false;
      }
      if (vm.armorFilters.selectedRarity.filter) {
        var filter = vm.armorFilters.selectedRarity.filter;
        if (filter === "Lower") {
          if (item.tierTypeName === "Exotic" || item.tierTypeName === "Legendary" || item.tierTypeName === "Rare") return false;
        }
        else if (filter != item.tierTypeName) return false;
      }
      if (!filterText(item)) return false;
      return true;

    };

    $scope.vm.saveToServer = function () {
      if (confirm("Are you sure you want to upload your marks to gear.destinychecklist.net? This could overwrite marks you already put on the site") == true) {
        //keep going
      } else {
        return;
      }
      
      var saveMe = {
        marked: $scope.vm.marked,
        notes: $scope.vm.notes,
        magic: "this is magic!",
        platform: $scope.platform.type,
        memberId: window.membershipId
      };
      var s = JSON.stringify(saveMe);
      var lzSaveMe = LZString.compressToBase64(s);
      var request = {
        method: 'POST',
        url: 'https://www.destinychecklist.net/api/mark',
        data: {
          data: lzSaveMe
        },
        dataType: 'json',
        withCredentials: true
      };

      $http(request).then(function success(response) {
        console.log("Success: ");
        console.dir(response);
      }, function failure(response) {
        console.log("Error: ");
        console.dir(response);
      });

    }

    $scope.vm.saveChecked = function () {
      $scope.vm.dirty = false;
      largeSync.set({"lzwMarked": $scope.vm.marked, "lzwNotes": $scope.vm.notes}, function () {
        console.log('Saved mark lzw');
      });
      //saveToServer();
    };
    $scope.vm.mark = function (tag, item) {
      if ($scope.vm.marked[item.instanceId] == tag) {
        delete $scope.vm.marked[item.instanceId];
        item.markClass = "";
        item.lbl = "";
      }
      else {
        $scope.vm.marked[item.instanceId] = tag;
        item.markClass = TAGS[tag].style;
        item.lbl = TAGS[tag].label;
      }
      $scope.vm.dirty = true;
    }

    $scope.vm.clickToOpen = function (item) {
      $scope.vm.modal = {
        item: item
      };
      var tDesc = CACHE.TalentGrid[item.base.talentGridHash];
      var steps = [];
      var selectedNodes = {};
      for (var cntr = 0; cntr < item.base.nodes.length; cntr++) {
        var node = item.base.nodes[cntr];
        selectedNodes[node.nodeHash] = node;
      }
      var maxCol = 0;
      for (var cntr = 0; cntr < tDesc.nodes.length; cntr++) {
        var node = tDesc.nodes[cntr];
        //skip nodes that aren't on this item
        //if (!selectedNodes[node.nodeHash]) continue;
        for (var cntr2 = 0; cntr2 < node.steps.length; cntr2++) {
          var step = node.steps[cntr2];
          var selNode = selectedNodes[node.nodeHash];
          if (selNode) {
            if (selNode.stepIndex == cntr2) {
              if (step.nodeStepName) {
                var pushMe = {
                  selected: selNode.isActivated,
                  col: node.column,
                  step: step
                }
                steps.push(pushMe);
                maxCol = Math.max(node.column, maxCol);
              }
            }
          }
        }
      }

      var cols = [];
      for (var cntr = 0; cntr <= maxCol; cntr++) {
        cols.push([]);
      }
      for (var cntr = 0; cntr < steps.length; cntr++) {
        var step = steps[cntr];
        if (step.col == -1) continue;
        cols[step.col].push(step);
      }
      $scope.vm.modal.cols = cols;
      //organize base and current stats
      for (var cntr = 0; cntr < item.jstats.length; cntr++) {
        var jstat = item.jstats[cntr];
        for (var cntr2 = 0; cntr2 < item.stats.length; cntr2++) {
          var stat = item.stats[cntr2];
          if (stat.name == jstat.name) {
            jstat.actual = stat.value;
          }
        }
      }

      item.jstats.sort(function (a, b) {
        if (a.name < b.name)
          return -1;
        else if (a.name > b.name)
          return 1;
        else
          return 0;
      });
      $('#itemModal').modal();
    }

    $scope.vm.setOptions = function (oItem, actionId, realChars) {

      //if it's equipped, no options
      if (oItem.equipped) {
        oItem.options = null;
        //nothing   
      }
      //if it's lost, no options
      else if (oItem.invisible) {
        oItem.options = null;
      }
      //if it's in vault, any char
      else if (actionId == "vault") {
        oItem.options = [];
        oItem.options = oItem.options.concat(realChars);
      }
      //if it's in char, equip or vault
      else {

        oItem.options = [];
        oItem.options.push({
          label: "Equip",
          id: "equip"
        });
        oItem.options.push({
          label: "Vault",
          id: "vault"
        });
      }
    };
    function fixLocks(items) {
      var updateList = [];
      var skipped = 0;
      for (var cntr = 0; cntr < items.length; cntr++) {
        var item = items[cntr];
        if (item.stale == true) {
          console.log(item.name + " is stale, skipping.");
          skipped++;
          continue;
        }
        var mark = $scope.vm.marked[item.instanceId];
        if (mark == "upgrade" || mark == "keep" || mark == "test") {
          if (!item.base.locked) {
            console.log("Lock " + item.name + ": " + mark);
            updateList.push(item);
          }
        }
        if (mark == "infuse" || mark == "junk") {
          if (item.base.locked) {
            console.log("Unlock " + item.name + ": " + mark);
            updateList.push(item);
          }
        }
      }
      var promises = [];
      for (var cntr = 0; cntr < updateList.length; cntr++) {
        var oItem = updateList[cntr];
        var newState = !oItem.base.locked;
        var item = buildPsuedoItem(oItem, $scope.vm.gear.realChars);
        item.name = oItem.name;
        var store = buildPsuedoStore(oItem, $scope.vm.gear.realChars);
        var promise = BungieService.setLockState(item, store, newState).then(function (response) {
          var msg;
          if (newState) msg = "Locked";
          else msg = "Unlocked";
          //toaster.pop('success', msg, oItem.name, 1000);
          toaster.pop({
            type: 'success',
            title: msg,
            body: item.name,
            timeout: 1000
          });

          oItem.stale = true;
          oItem.base.locked = newState;
        }).catch(function (err) {
          toaster.pop('error', '', err.message);
          console.dir(err);
        });
        promises.push(promise);
      }
      var finalPromise = $q.all(promises);
      finalPromise.then(function () {
        if (skipped > 0) {
          toaster.pop('warning', "Complete!", updateList.length + " items updated. " + skipped + " stale items were skipped. Run again to fix them too.", 3000);
          $scope.vm.loadGear();
        }
        else {
          toaster.pop('info', "Complete!", updateList.length + " items updated.", 1000);
          if (updateList.length > 0)
            $scope.vm.loadGear();
        }


      }).catch(function (err) {
        toaster.pop('error', '', err.message);
        console.dir(err);
      });
    };

    $scope.vm.fixWeaponLocks = function () {
      fixLocks($scope.vm.gear.gear.weapons);
    };

    $scope.vm.fixArmorLocks = function () {
      fixLocks($scope.vm.gear.gear.armor);
    };

    function buildPsuedoStore(item, realChars, targetOption) {
      var store = {
        isVault: false
      };
      if (targetOption) {
        store.id = targetOption.id;
        store.name = targetOption.label;
      }
      else {
        store.id = item.owner.id;
        store.name = item.owner.label;
      }

      if (store.id == "vault") store.isVault = true;
      return store;
    };

    function buildPsuedoItem(oItem, realChars) {
      var item = {
        id: oItem.instanceId,
        hash: oItem.hash,
        amount: 1,
        owner: oItem.owner.id
      };
      //send first char b/c Bungie is weird
      if (item.owner == "vault") {
        item.owner = realChars[0].id;
      }
      return item;
    };

    $scope.vm.toggleLockState = function (oItem) {
      var newState = !oItem.base.locked;
      console.log("setLockState " + oItem.name + ": " + newState);
      var item = buildPsuedoItem(oItem, $scope.vm.gear.realChars);
      var store = buildPsuedoStore(oItem, $scope.vm.gear.realChars);
      BungieService.setLockState(item, store, newState).then(function (response) {
        var msg;
        if (newState) msg = "Locked";
        else msg = "Unlocked";
        toaster.pop('success', msg, oItem.name, 1000);
        oItem.stale = true;
        oItem.base.locked = newState;
      }).catch(function (err) {
        toaster.pop('error', '', err.message);
        console.dir(err);
      });
    };

    $scope.vm.transfer = function (option, oItem) {
      console.log("Moving: " + oItem.name);

      var item = buildPsuedoItem(oItem, $scope.vm.gear.realChars);
      var store = buildPsuedoStore(oItem, $scope.vm.gear.realChars, option);

      if (option.id == "equip") {
        BungieService.equip(item).then(function (response) {
          toaster.pop('success', 'Equipped', oItem.name, 1000);
          oItem.stale = true;
          oItem.equipped = true;
          $scope.vm.setOptions(oItem, option.id, $scope.vm.gear.realChars);

        }).catch(function (err) {
          toaster.pop('error', '', err.message);
          console.dir(err);
        });
        return;
      }
      BungieService.transfer(item, store).then(function (response) {
        toaster.pop('success', 'Moved', oItem.name + " to " + option.label, 1000);
        oItem.stale = true;
        oItem.owner = option;
        $scope.vm.setOptions(oItem, option.id, $scope.vm.gear.realChars);
      }).catch(function (err) {
        toaster.pop('error', '', err.message);
        console.dir(err);
      });
    };

    $scope.vm.getPlatforms = function () {
      BungieService.getPlatforms().then(function (response) {
        var bungieUser = response.data.Response;
        if (bungieUser.gamerTag && bungieUser.psnId) {

          if (localStorage.preferredPlatform == 2) {
            $scope.platform = {
              id: bungieUser.psnId,
              type: 2,
              label: 'PSN',
              alternate: {
                id: bungieUser.gamerTag,
                type: 1,
                label: 'XBL'
              }
            };
          }
          else {
            $scope.platform = {
              id: bungieUser.gamerTag,
              type: 1,
              label: 'XBL',
              alternate: {
                id: bungieUser.psnId,
                type: 2,
                label: 'PSN'
              }
            };
          }
        }
        else if (bungieUser.gamerTag) {
          $scope.platform = {
            id: bungieUser.gamerTag,
            type: 1,
            label: 'XBL'
          };
        }
        else if (bungieUser.psnId) {
          $scope.platform = {
            id: bungieUser.psnId,
            type: 2,
            label: 'PSN'
          };
        }
        dimState.active = $scope.platform;

        $scope.vm.loggedIn = true;
        toaster.pop('success', 'Logon Success', "Logged in as " + $scope.platform.id + " on " + $scope.platform.label, 1000);
        if ($scope.vm.cacheReady && $scope.vm.loggedIn) $scope.vm.loadGear();
      }, function (reason) {
        toaster.pop('error', '', reason);
      });
    }

    $scope.vm.resetGlobalFilters = function () {
      $scope.vm.globalFilters = {
        tag: "any"
      };
    }

    $scope.vm.openDCTab = function () {
      var url = "http://www.destinychecklist.net/tabs/" + $scope.platform.type + "/" + window.encodeURIComponent($scope.platform.id);
      chrome.tabs.create({url: url});
    }


    $scope.vm.downloadWeaponCsv = function () {

      var header = "Name, Tier, Type, Tag, Light, Dmg, Owner, Leveled, Locked, Equipped, " +
        "AA, Impact, Range, Stability, ROF, Reload, Mag, Notes," +
        "Perks\n";
      var guns = $scope.vm.gear.gear.weapons;
      var data = "";
      for (var cntr = 0; cntr < guns.length; cntr++) {
        var gun = guns[cntr];
        data += gun.name + ", ";
        data += gun.tierTypeName + ", ";
        data += gun.itemTypeName + ", ";
        data += gun.lbl + ", ";
        data += gun.light + ", ";
        data += gun.damageType + ", ";
        data += gun.owner.label + ", ";
        data += gun.leveled + ", ";
        data += gun.base.locked + ", ";
        data += gun.equipped + ", ";
        data += gun.aa + ", ";
        data += gun.impact + ", ";
        data += gun.range + ", ";
        data += gun.stability + ", ";
        data += gun.rof + ", ";
        data += gun.reload + ", ";
        data += gun.magazine + ", ";
        if ($scope.vm.notes[gun.instanceId])
          data += $scope.vm.notes[gun.instanceId] + ", ";
        else data += ", ";
        for (var cntr2 = 0; cntr2 < gun.steps.length; cntr2++) {
          var step = gun.steps[cntr2];
          data += step.step.nodeStepName;
          if (step.selected) data += "*";
          data += ", ";
        }
        data += "\n";
      }


      downloadCsv("destinyWeapons", header + data);
    }

    function updateSearchText(item) {
      var searchText = item.name + " " + item.searchTypeName + " " + $scope.vm.notes[item.instanceId];
      if (item.steps) {
        for (var stepCntr = 0; stepCntr < item.steps.length; stepCntr++) {
          var step = item.steps[stepCntr];
          searchText += " " + step.step.nodeStepName;
        }
      }
      item.searchText = searchText.toUpperCase();
    }

    $scope.vm.updateSearchText = function (oItem) {
      updateSearchText(oItem);
      vm.dirty = true;
    }


    $scope.vm.downloadArmorCsv = function () {

      //x name, tier, type, tag, eqippabled by, light, owner, level, locked, equipped
      //
      //roll %, int %, disc %, str %, int, disc, str,
      //    selected perks
      //perks


      var header = "Name, Tier, Type, Tag, Eqippable, Light, Owner, Leveled, Locked, Equipped, " +
        "Split, Quality, IntQ, DiscQ, StrQ, Int, Disc, Str, Notes, Perks\n";
      var guns = $scope.vm.gear.gear.armor;
      var data = "";
      for (var cntr = 0; cntr < guns.length; cntr++) {
        var arm = guns[cntr];
        data += arm.name + ", ";
        data += arm.tierTypeName + ", ";
        data += arm.itemTypeName + ", ";
        data += arm.lbl + ", ";
        data += arm.classAllowed + ", ";
        data += arm.light + ", ";
        data += arm.owner.label + ", ";
        data += arm.leveled + ", ";
        data += arm.base.locked + ", ";
        data += arm.equipped + ", ";
        data += arm.statSplit + ", ";
        data += arm.qualityPct + ", ";
        data += arm.intPct + ", ";
        data += arm.disPct + ", ";
        data += arm.strPct + ", ";
        data += arm.int + ", ";
        data += arm.disc + ", ";
        data += arm.str + ", ";
        if ($scope.vm.notes[arm.instanceId])
          data += $scope.vm.notes[arm.instanceId] + ", ";
        else data += ", ";
        for (var cntr2 = 0; cntr2 < arm.steps.length; cntr2++) {
          var step = arm.steps[cntr2];
          data += step.step.nodeStepName;
          if (step.selected) data += "*";
          data += ", ";
        }
        data += "\n";
      }


      downloadCsv("destinyArmor", header + data);
    }

    function downloadCsv(filename, csv) {
      var filename = filename + ".csv";
      var pom = document.createElement('a');
      pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv));
      pom.setAttribute('download', filename);
      pom.click();
    };


    function decodeDamage(type) {
      if (type == 0)
        return "Kinetic";
      else if (type == 1)
        return "Kinetic";
      else if (type == 2)
        return "Arc";
      else if (type == 3)
        return "Solar";
      else if (type == 4)
        return "Void";
      else
        return "";
    }

    function handleBuckets(char, gear, buckets, realChars, bInvisible) {
      for (var cntr3 = 0; cntr3 < buckets.length; cntr3++) {
        var items = buckets[cntr3].items;

        for (var cntr = 0; cntr < items.length; cntr++) {
          var item = items[cntr];

          var jDesc = CACHE.InventoryItem[item.itemHash];
          //no def, don't break things
          if (!jDesc){
            console.log("Missing desc for "+item.itemHash)
            return;
          }
          if (jDesc.itemType != 2 && jDesc.itemType != 3 && jDesc.itemType != 8) continue;

          var impact, range, stability, rof, reload, magazine, int, disc, str;
          for (var cntr6 = 0; cntr6 < item.stats.length; cntr6++) {
            if (item.stats[cntr6].statHash == 1240592695)
              range = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 4043523819)
              impact = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 155624089)
              stability = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 4284893193 && jDesc.itemTypeName != "Fusion Rifle")
              rof = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 2961396640 && jDesc.itemTypeName == "Fusion Rifle")
              rof = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 4188031367)
              reload = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 3871231066)
              magazine = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 144602215)
              int = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 1735777505)
              disc = item.stats[cntr6].value;
            else if (item.stats[cntr6].statHash == 4244567218)
              str = item.stats[cntr6].value;
          }
          var marked = vm.marked[item.itemInstanceId];
          var markClass = "";
          var lbl = "";
          if (marked) {

            if (marked == "upgrade") {
              markClass = "info";
              lbl = "Upgrade Me";
            }
            else if (marked == "keep") {
              markClass = "success";
              lbl = "Keep Me";
            }
            else if (marked == "test") {
              markClass = "active";
              lbl = "Test Me";
            }
            else if (marked == "infuse") {
              markClass = "warning";
              lbl = "Infusion Fuel";
            }
            else if (marked == "junk") {
              markClass = "danger";
              lbl = "Dismantle Me";
            }

          }

          var options = [];

          var classAllowed = "Any";
          if (jDesc.classType == 0) {
            classAllowed = "Titan";
          }
          else if (jDesc.classType == 1) {
            classAllowed = "Hunter";
          }
          else if (jDesc.classType == 2) {
            classAllowed = "Warlock";
          }


          var steps = [];
          if (item.progression) {
            var tDesc = CACHE.TalentGrid[item.talentGridHash];
            var selectedNodes = {};
            for (var cntr7 = 0; cntr7 < item.nodes.length; cntr7++) {
              var node = item.nodes[cntr7];
              selectedNodes[node.nodeHash] = node;
            }

            for (var cntr7 = 0; cntr7 < tDesc.nodes.length; cntr7++) {
              var node = tDesc.nodes[cntr7];
              if (node.column < 1) continue;

              for (var cntr8 = 0; cntr8 < node.steps.length; cntr8++) {
                var step = node.steps[cntr8];
                step.col = node.column;
                var selNode = selectedNodes[node.nodeHash];
                if (selNode) {
                  if (selNode.stepIndex == cntr8) {
                    if (step.nodeStepName) {
                      var pushMe = {
                        selected: selNode.isActivated,
                        step: step
                      }
                      steps.push(pushMe);
                    }
                  }
                }
              }
            }
          }

          steps.sort(function (a, b) {
            return a.step.col - b.step.col;
          });

          var oItem = {
            invisible: bInvisible ? true : false,
            owner: char,
            int: int,
            disc: disc,
            str: str,
            damageType: decodeDamage(item.damageType),
            classAllowed: classAllowed,
            bucket: CACHE.InventoryBucket[buckets[cntr3].bucketHash].bucketName,
            bucket2: CACHE.InventoryBucket[jDesc.bucketTypeHash].bucketName,
            hash: item.itemHash,
            itemType: jDesc.itemType,
            itemSubType: jDesc.itemSubType,
            tierTypeName: jDesc.tierTypeName,
            itemTypeName: bInvisible ? "Postmaster" : jDesc.itemTypeName,
            searchTypeName: jDesc.itemTypeName,
            name: jDesc.itemName,
            aa: jDesc.stats[1345609583] ? jDesc.stats[1345609583].value : 0,
            inventory: jDesc.stats[1931675084] ? jDesc.stats[1931675084].value : 0,
            reload: reload,
            rof: rof,
            magazine: magazine,
            impact: impact, //jDesc.stats[4043523819] ? jDesc.stats[4043523819].value : 0,
            range: range, //jDesc.stats[1240592695] ? jDesc.stats[1240592695].value : 0,
            stability: stability,
            leveled: item.isGridComplete,
            instanceId: item.itemInstanceId,
            equipped: item.isEquipped,
            options: options,
            stats: [],
            jstats: [],
            steps: steps,
            base: item,
            icon: jDesc.icon,
            //template: jDesc,
            markClass: markClass,
            lbl: lbl
          };
          updateSearchText(oItem);
          $scope.vm.setOptions(oItem, char.id, realChars);

          if (item.stats) {
            for (var cntr4 = 0; cntr4 < item.stats.length; cntr4++) {
              var stat = item.stats[cntr4];
              var statDef = CACHE.Stat[stat.statHash];
              oItem.stats.push({
                name: statDef.statName,
                value: stat.value
              });
            }
          }
          if (jDesc.stats) {
            for (var dictKey in jDesc.stats) {
              if (jDesc.stats.hasOwnProperty(dictKey)) {
                var entry = jDesc.stats[dictKey];
                oItem.jstats.push({
                  hash: entry.statHash,
                  name: CACHE.Stat[entry.statHash].statName,
                  value: entry.value
                });
              }
            }
          }

          //weapons
          if (jDesc.itemType == 3) {
            if (item.primaryStat) oItem.light = item.primaryStat.value;
            else oItem.light = 0;
            gear.weapons.push(oItem);
          }
          //armor
          else if (jDesc.itemType == 2) {
            if (item.primaryStat) oItem.light = item.primaryStat.value;
            else oItem.light = 0;
            ItemQualityService.setQualityRating(oItem);
            gear.armor.push(oItem);

          }
          //engrams
          else if (jDesc.itemType == 8) {
            gear.engrams.push(oItem);

          }
        }
      }

    }

    $scope.vm.loadGear = function () {
      if (!$scope.vm.cacheReady) {
        toaster.pop('error', '', "Manifest data not loaded");
        return;
      }
      if (!$scope.vm.loggedIn) {
        toaster.pop('error', '', "Not logged in");
        return;
      }
      $scope.vm.loading = true;
      $('#loadingModal').modal();
      BungieService.getStores($scope.platform).then(function (response) {
        $scope.vm.stores = response;

        if ($scope.vm.gear) {
          $scope.vm.gear.chars = [];
          $scope.vm.gear.gear.weapons = [];
          $scope.vm.gear.gear.armor = [];
          $scope.vm.gear.gear.engrams = [];
        }
        else {
          $scope.vm.gear = {
            chars: [],
            gear: {
              weapons: [],
              weaponSort: 'light',
              weaponReverse: true,
              armor: [],
              armorSort: 'light',
              armorReverse: true,
              engrams: [],
              engramSort: 'tierTypeName',
              engramReverse: false
            }
          };
        }
        $scope.vm.gear.realChars = [];
        $scope.vm.marks = 0;
        $scope.vm.glimmer = 0;
        for (var cntr = 0; cntr < response.length; cntr++) {
          if (response[cntr].character.base) {
            if (response[cntr].character.base.inventory) {
              var currs = response[cntr].character.base.inventory.currencies;
              for (var cntr2 = 0; cntr2 < currs.length; cntr2++) {
                if (3159615086 == currs[cntr2].itemHash)
                  $scope.vm.glimmer = currs[cntr2].value;
                else if (2534352370 == currs[cntr2].itemHash)
                  $scope.vm.marks = currs[cntr2].value;
              }
            }

            var char = response[cntr].character.base.characterBase;

            var oChar = {
              id: response[cntr].character.id,
              raceName: CACHE.Race[char.raceHash].raceName,
              genderName: CACHE.Gender[char.genderHash].genderName,
              className: CACHE.Class[char.classHash].className,
              light: char.powerLevel,
              icon: response[cntr].character.base.emblemPath
            };
            oChar.label = oChar.className + "(" + oChar.light + ")";
            //" " + oChar.raceName.substr(0, 2) + " " + oChar.genderName.substr(0, 1);
            $scope.vm.gear.chars.push(oChar);
            $scope.vm.gear.realChars.push(oChar);

          }
          else {
            var oChar = {
              id: "vault",
              raceName: "",
              genderName: "",
              className: "",
              light: 0,
              label: "Vault"
            };
            $scope.vm.gear.chars.push(oChar);
          }
        }

        for (var cntr = 0; cntr < $scope.vm.gear.chars.length; cntr++) {
          var oChar = $scope.vm.gear.chars[cntr];
          if (oChar.label != "Vault") {

            handleBuckets(oChar, $scope.vm.gear.gear, response[cntr].data.buckets.Equippable, $scope.vm.gear.realChars);
            handleBuckets(oChar, $scope.vm.gear.gear, response[cntr].data.buckets.Invisible, $scope.vm.gear.realChars, true);
            handleBuckets(oChar, $scope.vm.gear.gear, response[cntr].data.buckets.Item, $scope.vm.gear.realChars);
          }
          else {
            handleBuckets(oChar, $scope.vm.gear.gear, response[cntr].data.buckets, $scope.vm.gear.realChars);
          }
        }

        function cleanupMissingItems(title, checkMe) {
          var usedKeys = {};
          var totalKeys = 0, missingKeys = 0;
          //copy create data struct of all the marked items
          for (var key in checkMe) {
            if (checkMe.hasOwnProperty(key)) {
              usedKeys[key] = false;
              totalKeys++;
            }
          }
          //for all the marked items that are in the current gear, mark true
          var arr = $scope.vm.gear.gear.weapons;
          for (var cntr = 0; cntr < arr.length; cntr++) {
            var item = arr[cntr];
            if (usedKeys[item.instanceId] != null)
              usedKeys[item.instanceId] = true;
          }
          arr = $scope.vm.gear.gear.armor;
          for (var cntr = 0; cntr < arr.length; cntr++) {
            var item = arr[cntr];
            if (usedKeys[item.instanceId] != null)
              usedKeys[item.instanceId] = true;
          }
          arr = $scope.vm.gear.gear.engrams;
          for (var cntr = 0; cntr < arr.length; cntr++) {
            var item = arr[cntr];
            if (usedKeys[item.instanceId] != null)
              usedKeys[item.instanceId] = true;
          }
          var unusedDelete = false;
          for (var key in usedKeys) {
            if (usedKeys.hasOwnProperty(key)) {
              //if it wasn't found, it's left over and should be deleted
              if (usedKeys[key] == false) {
                console.log("Deleting unused key: " + key);
                delete checkMe[key];
                unusedDelete = true;
                missingKeys++;
              }
            }
          }

          if (unusedDelete) {
            $scope.vm.saveChecked();
          }
          console.log(title + ": " + missingKeys + " unused out of total " + totalKeys);
        }

        //cleanup missing entries
        if ($scope.vm.marked) {
          cleanupMissingItems("Markings", $scope.vm.marked)
        }
        if ($scope.vm.notes) {
          cleanupMissingItems("Notes", $scope.vm.notes)
        }

        toaster.pop('success', '', "Gear loaded", 1000);
        $scope.vm.filterItems($scope.vm);
        $scope.vm.loading = false;
        $('#loadingModal').modal('hide');
        $scope.vm.getFoundryOrders();
      }).catch(function (e) {
        console.dir(e);
        toaster.pop('error', '', e.message);
        $scope.vm.loading = false;
        $('#loadingModal').modal('hide');
      });

    }

    $scope.vm.test = function () {
      console.log("Test");
    }

    $scope.vm.postProcessGuns = function () {
      var guns = {};

      var totalScore = 0;
      var totalKills = 0;
      var totalDeaths = 0;
      var totalAssists = 0;
      $scope.vm.pvp.forEach(function (match) {

        //filter
        var fireTeamOption = $scope.vm.pvpFilters.selectedFireTeam;
        if ("solo" === fireTeamOption) {
          if (match.fireTeamSize > 1) return;
        }
        else if ("team" === fireTeamOption) {
          if (match.fireTeamSize <= 1) return;
        }
        var mapOption = $scope.vm.pvpFilters.selectedMap;
        if (mapOption && mapOption != "any") {
          if (match.name != mapOption) return;
        }

        totalDeaths += match.values.deaths.basic.value;
        ;
        totalKills += match.values.kills.basic.value;
        totalAssists += match.values.assists.basic.value;
        totalScore += match.values.score.basic.value;

        match.weapons.forEach(function (gun) {
          if (!guns[gun.id]) {
            guns[gun.id] = {
              id: gun.id,
              name: gun.name,
              icon: gun.icon,
              kills: 0,
              deaths: 0,
              assists: 0,
              actualKills: 0,
              precisionKills: 0,
              data: []
            }
          }
          guns[gun.id].data.push({
            kd: match.kd,
            kda: match.kda,
            kills: match.values.kills.basic.value,
            deaths: match.values.deaths.basic.value,
            assists: match.values.assists.basic.value,
            score: match.values.score.basic.value,
            gunKills: gun.kills,
            gunPrecisionKills: gun.precision
          });

        });
      });

      if (totalDeaths == 0) totalDeaths = 1;
      var avgScore = (totalScore / $scope.vm.pvp.length);
      var avgKD = (totalKills / totalDeaths);
      var avgKDA = ((totalKills + totalAssists) / totalDeaths);
      $scope.vm.pvpAvg = {
        score: avgScore,
        kd: avgKD,
        kda: avgKDA
      };

      var aGuns = [];
      angular.forEach(guns, function (gun) {
        var totalScore = 0;
        gun.data.forEach(function (point) {
          gun.kills += point.kills;
          gun.deaths += point.deaths;
          gun.assists += point.assists;
          gun.actualKills += point.gunKills;
          gun.precisionKills += point.gunPrecisionKills;
          totalScore += point.score;
        });
        gun.kd = (gun.kills / gun.deaths);
        gun.kda = ((gun.kills + gun.assists) / gun.deaths);
        gun.kpg = (gun.actualKills / gun.data.length);
        gun.precPct = (gun.precisionKills / gun.actualKills);
        gun.avgScore = (totalScore / gun.data.length);
        aGuns.push(gun);
      });
      $scope.vm.pvpGuns = aGuns;
    }

    function pv(values, key) {
      if (!values) return ",";
      if (!values[key]) return ",";
      var val = values[key].basic.displayValue;
      val = val.replace(",", "");
      return val + ",";
    }

    $scope.vm.downloadPvpCsv = function (char) {

      $('#loadingModal').modal();
      PvPService.getPvpHistoryForChar($scope.platform.type,
        window.membershipId,
        char,
        $scope.vm.pvpFilters.selectedMode.filter,
        50000, $scope.vm)
        .then(function (matches) {
          console.log(matches.length);
          $('#loadingModal').modal('hide');
          vm.statusMsg = "";
          toaster.pop('success', '', 'Found ' + matches.length + " matches for " + char.label, 2000);
          var header = "Date, Type, Map,";
          header += "KDR,KDAR,Kills,Deaths,Assists,Score,Standing,Team Score," +
            "Private, averageScorePerKill,averageScorePerLife," +
            "completed,activityDuration," +
            "team,playerCount,leaveRemainingSeconds,completionReason,activityDurationS,URL";
          header += "\n";
          var data = "";
          matches.forEach(function (match) {
            var period = moment(match.period);
            var displayPeriod = period.format('YYYY-MM-DD HH:mm');
            var hash = match.activityDetails.referenceId;
            var typeHash = match.activityDetails.activityTypeHashOverride;
            var aDesc = CACHE.Activity[hash];
            var pvpType = typeHash > 0 ? CACHE.ActivityType[typeHash].activityTypeName : "Custom";
            var name = aDesc.activityName;
            data += displayPeriod + "," + pvpType + "," + name + ",";
            if (match.values) {
              var v = match.values;
              data += pv(v, "killsDeathsRatio");
              data += pv(v, "killsDeathsAssists");
              data += pv(v, "kills");
              data += pv(v, "deaths");
              data += pv(v, "assists");
              data += pv(v, "score");
              data += pv(v, "standing");
              data += pv(v, "teamScore");
              data += match.activityDetails.isPrivate + ",";
              data += pv(v, "averageScorePerKill");
              data += pv(v, "averageScorePerLife");
              data += pv(v, "completed");
              data += pv(v, "activityDurationSeconds");
              data += pv(v, "team");
              data += pv(v, "playerCount");
              data += pv(v, "leaveRemainingSeconds");
              data += pv(v, "completionReason");
              data += match.values.activityDurationSeconds.basic.value + ",";
            }
            data += '=HYPERLINK("https://www.destinychecklist.net/pvpmatch/' + match.activityDetails.instanceId + '")';
            data += "\n";
          });
          downloadCsv("pvpHistory-" + char.label, header + data);
        })
        .catch(function (e) {
          $scope.vm.loading = false;
          $('#loadingModal').modal('hide');
          toaster.pop('error', '', e.message);
        });

    }

    $scope.vm.loadPvp = function () {
      $scope.vm.pvpGuns = null;
      $scope.vm.pvpFilters.selectedFireTeam = "any";
      $scope.vm.pvpFilters.selectedMap = "any";
      $scope.vm.pvpAvg = null;
      $scope.vm.pvpProgress = {
        state: 'history',
        completed: 0,
        total: 1,
        pct: '0%'
      };

      var searchChars;
      if ($scope.vm.pvpFilters.selectedChar.id == null) {
        searchChars = $scope.vm.gear.realChars;
      }
      else {
        searchChars = [$scope.vm.pvpFilters.selectedChar];
      }

      var prom = PvPService.getPvpHistory2($scope.platform.type,
        window.membershipId,
        searchChars,
        $scope.vm.pvpFilters.selectedMode.filter,
        $scope.vm.pvpFilters.selectedCount.filter
      );
      prom
        .then(function (response) {
          var matches = [];
          var omaps = {};
          response.forEach(function (charAct) {
            if (!charAct.activities) return;
            charAct.activities.forEach(function (match) {
              match.character = charAct.character;
              var period = moment(match.period);
              match.displayPeriod = period.format('MM/DD/YY kk:mm');
              var kills = match.values.kills.basic.value;
              var deaths = match.values.deaths.basic.value;
              if (kills == 0 && deaths == 0) return;
              var assists = match.values.assists.basic.value;

              var kd = kills / deaths;
              var kda = (kills + assists) / deaths;
              match.kd = kd;
              match.kda = kda;

              var hash = match.activityDetails.referenceId;
              var typeHash = match.activityDetails.activityTypeHashOverride;
              var aDesc = CACHE.Activity[hash];
              var pvpType = typeHash > 0 ? CACHE.ActivityType[typeHash].activityTypeName : "Custom";
              var name = aDesc.activityName;
              match.name = name;
              omaps[name] = true;
              match.mode = pvpType;
              matches.push(match);
            });
          });
          var maps = [];
          angular.forEach(omaps, function (value, key) {
            maps.push(key);
          });
          maps.sort();

          $scope.vm.pvpProgress.total = matches.length;
          $scope.vm.pvpSort = "period";
          $scope.vm.pvpReverse = true;
          $scope.vm.pvpGunSort = "kd";
          $scope.vm.pvpGunReverse = true;
          $scope.vm.pvpGunThreshold = 0;

          function compare(a, b) {
            if (a.period < b.period)
              return 1;
            if (a.period > b.period)
              return -1;
            return 0;
          }

          matches.sort(compare);


          var tasks = [];
          matches.forEach(function (match) {
            tasks.push(function () {
              return PvPService.getPGCR(PGCR_CACHE, $scope.vm.pvpProgress, match);
            });
          });
          $scope.vm.pvp = matches;
          $scope.vm.pvpmaps = maps;

          $scope.vm.pvpProgress.state = "PGCR";

          $q.serial(tasks).then(function () {
            $scope.vm.postProcessGuns();
            savePGCRToStorage();
            if (matches && matches.length > 0) {
              if (matches[0].missingData) {
                toaster.pop('warning', '', 'Extended gun data missing from Bungie, detailed gun reports currently unavailable', 5000);
              }
            }
            toaster.pop('success', '', 'All matches loaded!', 1000);
            $scope.vm.pvpProgress = null;
          });

        })
        .catch(function(err){
          toaster.pop('error', '', err.message);
          console.dir(err);
          $scope.vm.pvpProgress = null;
        });

    }

    function finishInit() {

      $('#loadingModal').modal();
      if (!$scope.vm.marked) {
        console.log("Storage was empty");
        $scope.vm.marked = {};
      }
      if (!$scope.vm.notes) {
        console.log("Storage notes were empty");
        $scope.vm.notes = {};
      }
      $scope.vm.getPlatforms();
      Manifest.getDefinitions().then(function (defs) {
        CACHE = defs;
        $scope.vm.cache = defs;
        $scope.vm.cacheReady = true;
        toaster.pop('success', '', 'Definitions loaded!', 1000);
        if ($scope.vm.cacheReady && $scope.vm.loggedIn) $scope.vm.loadGear();
      });
    }

    $scope.vm.clearPGCRCache = function () {
      PGCR_CACHE = {};
      var saveMe = {"PGCR": null};
      chrome.storage.local.set(saveMe, function (obj) {
        console.log("Cleared local storage");

        $('#clearCacheModal').modal('hide');
        toaster.pop({
          type: 'success',
          body: "PGCR Cache cleared",
          timeout: 2000
        });
      });
    }

    function byteString(bytes, precision) {
      if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
      if (typeof precision === 'undefined') precision = 1;
      var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
        number = Math.floor(Math.log(bytes) / Math.log(1024));
      return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }

    $scope.vm.showCacheClearModal = function () {
      $scope.clear = {
        cacheCount: Object.keys(PGCR_CACHE).length,

      };
      chrome.storage.local.getBytesInUse("PGCR", function (bytesInUse) {
        var s = byteString(bytesInUse);
        $("#cacheSize").html(s);
        $('#clearCacheModal').modal();
      });
    }

    function savePGCRToStorage() {
      if (PGCR_CACHE.dirty) {
        delete PGCR_CACHE.dirty;
        var s = JSON.stringify(PGCR_CACHE);
        s = LZString.compressToBase64(s);

        var saveMe = {"PGCR": s};
        chrome.storage.local.set(saveMe, function (obj) {
          console.log("Saved to local storage");
        });
      }
    }

    function processNodes(talentGridHash, damageType, nodes) {

      var steps = [];
      var tDesc = CACHE.TalentGrid[talentGridHash];
      var selectedNodes = {};
      for (var cntr7 = 0; cntr7 < nodes.length; cntr7++) {
        var node = nodes[cntr7];
        selectedNodes[node.nodeHash] = node;
      }
      for (var cntr7 = 0; cntr7 < tDesc.nodes.length; cntr7++) {
        var node = tDesc.nodes[cntr7];
        if (node.column < 1) continue;

        for (var cntr8 = 0; cntr8 < node.steps.length; cntr8++) {
          var step = node.steps[cntr8];
          step.col = node.column;
          var selNode = selectedNodes[node.nodeHash];
          if (selNode) {
            if (selNode.stepIndex == cntr8) {
              if (step.nodeStepName) {
                steps.push(step);
              }
            }
          }
        }
      }

      steps.sort(function (a, b) {
        return a.col - b.col;
      });

      var groups = {};
      steps.forEach(function (step) {
        if (!groups[step.col]) {
          groups[step.col] = [];
        }
        groups[step.col].push(step.nodeStepName);
      });
      var aGroups = [];
      angular.forEach(groups, function (group) {
        aGroups.push(group);
      });
      return {
        damageType: decodeDamage(damageType),
        steps: aGroups
      };
    }


    $scope.launchCopyModal = function () {
      var foundryRolls = $scope.vm.foundryOrders;
      if (!foundryRolls) return;
      var text = "___\n\n";
      foundryRolls.forEach(function (gun) {
        text += "**" + gun.name + " - " + gun.itemTypeName + "**\n\n";
        gun.damageAndSteps.forEach(function (roll, index, arr) {
          text += (index + 1) + ". ";
          roll.steps.forEach(function (step, index, arr) {
            step.forEach(function (node, index, arr) {
              text += node;
              if (index < (arr.length - 1)) {
                text += " / "
              }
            });
            if (index < (arr.length - 1)) {
              text += " - "
            }
          });
          text += "\n";
        });
        text += "\n";
        text += "___\n";
      });
      $scope.vm.redditRolls = text;
      $('#copyModal').modal();
    }

    $scope.vm.getFoundryOrders = function () {

      BungieService.getFoundryOrders($scope.vm.gear.realChars).then(function (charGuns) {

        var pureGunsFound = false;
        var pureGuns = {};
        var expires = null;

        var expires = null;

        var transmitGuns = [];
        var displayGuns = [];

        charGuns.forEach(function (charGun) {
          expires = charGun.expires;
          var owner = charGun.owner;
          charGun.guns.forEach(function (gun) {
            if (gun.rolls.length >= 3) {
              transmitGuns.push({
                itemHash: gun.itemHash,
                talentGridHash: gun.talentGridHash,
                rolls: gun.rolls
              });
            }
            var jDesc = CACHE.InventoryItem[gun.itemHash];

            var damageAndSteps = [];
            gun.rolls.forEach(function (roll) {
              var pushMe = processNodes(gun.talentGridHash, roll.damageType, roll.nodes);
              damageAndSteps.push(pushMe);
            });

            displayGuns.push({
              itemHash: gun.itemHash,
              itemTypeName: jDesc.itemTypeName,
              tierTypeName: jDesc.tierTypeName,
              name: jDesc.itemName,
              owner: owner,
              damageAndSteps: damageAndSteps
            });
          });
        });

        var transmitGuns = _.uniq(transmitGuns, function (item, key, a) {
          return item.itemHash;
        });
        console.log(transmitGuns.length);
        $scope.vm.foundryOrders = displayGuns;

        if (transmitGuns.length > 0) {

          var o = {
            magic: "this is magic!",
            guns: transmitGuns,
            expires: expires
          };

          var s = JSON.stringify(o);
          var lzGuns = LZString.compressToBase64(s);
          var request = {
            method: 'POST',
            url: 'https://www.destinychecklist.net/api/guns',
            data: {
              data: lzGuns
            },
            dataType: 'json',
            withCredentials: true
          };

          $http(request).then(function success(response) {
            console.log("Success: ");
            console.dir(response);
          }, function failure(response) {
            console.log("Error: ");
            console.dir(response);
          });
        }
      });

    }

    $scope.switchPlatform = function () {
      console.log("Switch platform");
      var tempId = $scope.platform.id;
      var tempType = $scope.platform.type;
      var tempLabel = $scope.platform.label;
      $scope.platform.id = $scope.platform.alternate.id;
      $scope.platform.type = $scope.platform.alternate.type;
      $scope.platform.label = $scope.platform.alternate.label;
      $scope.platform.alternate.id = tempId;
      $scope.platform.alternate.type = tempType;
      $scope.platform.alternate.label = tempLabel;
      $scope.vm.loadGear();
      localStorage.preferredPlatform = $scope.platform.type;
    }
    //
    //function getSyncLabel(){
    //  return $scope.platform.label+" "+$scope.platform.id;
    //}

    //TODO sync data by member id and platform not globally

    $scope.vm.paypal = function ($event) {
      $($event.target).closest('form').submit();
    };

    largeSync.get(["lzwMarked", "lzwNotes"], function (obj, err) {
      if (err) {
        console.log("Largesync is corrupted. Clearing.")
        chrome.storage.sync.clear();
        finishInit();
        return;
      }
      //fallback to marked
      if (!obj.lzwMarked) {
        chrome.storage.sync.get('marked', function (obj) {
          if (typeof obj.marked === 'string') console.log("String instead of object, ignoring");
          else $scope.vm.marked = obj.marked;
          console.log('marked' + JSON.stringify($scope.vm.marked));
          finishInit();
        });
      }
      else {
        $scope.vm.marked = obj.lzwMarked;
        $scope.vm.notes = obj.lzwNotes;
        console.log('loaded from LZW');
        finishInit();
      }
    });

    chrome.storage.local.get("PGCR", function (obj) {
      if (!obj) return;
      var s = obj["PGCR"];
      if (s) {
        PGCR_CACHE = JSON.parse(LZString.decompressFromBase64(s));
      }
    });


    $interval(function () {
      if ($scope.vm.dirty) {
        $scope.vm.saveChecked();
        toaster.pop('info', '', "Autosaving marks", 1000);
      }
    }, 20000);


  }
})
();
/**
 * Created by Dave on 12/25/2015.
 */
