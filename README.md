# DestinyChecklist.net Spreadsheet Manager


To install the production version of the extension, download the extension from the [Chrome Store](https://chrome.google.com/webstore/detail/destinychecklistnet-spead/feapilfcgikmmcdhledhnaefdjjfdomm).

##Requirements
This is an extension that runs within the Chrome Desktop Web Browser.

##Quick start

Clone the repo:

* `git clone https://dcaslin@bitbucket.org/dcaslin/destinyextension.git`

You can run now run DIM locally by enabling [Chrome Extensions Developer Mode](https://developer.chrome.com/extensions/faq#faq-dev-01) 
and point to the folder where you cloned.

##License
Code released under [the MIT license](http://choosealicense.com/licenses/mit/).