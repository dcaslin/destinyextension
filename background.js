

// Open tab
chrome.browserAction.onClicked.addListener(function () {

    var wUrl = chrome.extension.getURL("window.html");
    chrome.tabs.query({
        url: wUrl
    }, function (t) {
        if (t.length == 0) {
            console.log("Creating");
            chrome.tabs.create({
                url: wUrl
            });
        } else {
            console.log("Activating");
            chrome.tabs.update(t[0].id, {
                active: true
            });
        }

    });
});

chrome.webRequest.onBeforeSendHeaders.addListener(details => {
    let headers = details.requestHeaders.filter(h => {
          return !(h.name === "Origin" && h.value.startsWith("chrome-extension://"));
});
return {requestHeaders: headers};
},
{ urls: ["*://*.bungie.net/Platform/*"] },
["blocking", "requestHeaders"]
);